const url = 'https://localhost:7169/';

async function start() {
  let resp = await fetch(url + 'getdoctors').then
    (ress => { return ress.json(); });
  let docselector = document.getElementById('Select-Doctor');
  console.log(resp['data']);
  for (var i = 0; i < resp.data.length; i++) {
    var opt = document.createElement('option');
    opt.value = resp['data'][i]['id'];
    opt.innerText = resp['data'][i]['fio'];
    docselector.appendChild(opt);
  }

}

start();

console.log(document.getElementById('Select-Time').options);

document.getElementById('Select-Doctor').onchange = async (e) => {
  if (document.getElementById('Select-Date').value === '') {
    return;
  }
  loadTimes(e);
}


document.getElementById('Select-Date').onchange = async (e) => {
  if (document.getElementById('Select-Doctor').value === '') {
    return;
  }
  loadTimes(e);
}

async function loadTimes(e) {
  var timeselect = document.getElementById('Select-Time');
  var doctorselect = document.getElementById('Select-Doctor');
  var dateselect = document.getElementById('Select-Date');
  timeselect.setAttribute('readonly','');
  doctorselect.setAttribute('readonly','');
  dateselect.setAttribute('readonly','');
  var i, L = timeselect.options.length - 1;
  for (i = L; i >= 0; i--) {
    timeselect.remove(i);
  }
  var response = await fetch(url + 'getreservedtimes?docId=' + document.getElementById('Select-Doctor').value + "&date=" + e.target.value).then
    (ress => { return ress.json(); });
  for (var i = 0; i < response.length; i++) {
    var opt = document.createElement('option');
    opt.value = response[i];
    opt.innerText = response[i];
    timeselect.appendChild(opt);
  }
  timeselect.removeAttribute('readonly');
  doctorselect.removeAttribute('readonly');
  dateselect.removeAttribute('readonly');
}


document.getElementById('RegForm').onsubmit = async (e) => {
  e.preventDefault();
  let subbutton = document.getElementById('SubmitButton');
  subbutton.classList.add("text-center");
  subbutton.innerHTML = "<button class=\"btn btn-primary mb-3\" type=\"button\" disabled><span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span><span class=\"sr-only\">Зачекайте будь-ласка</span></button>";
  let response = await fetch(url + 'createTicket', {
    method: 'POST',
    body: new FormData(document.getElementById('RegForm')),
  }).then(res => {
    return res.json();
  });
  console.log(response);
  let regSection = document.getElementById('RegSection');
  if (response['Success'] === false) {
    regSection.innerHTML = "<div style=\"width: 350px; height: 575.5px;\" class=\"d-flex flex-column align-items-center bg-light rounded shadow-lg position-relative\"><legend class=\"text-center border-bottom shadow-sm text-danger\">Нажаль не вдалося створити<br>електроний запис до лікаря.</legend><div><b style=\"font-size: 24px;\" id=\"errortext\"></b></div><div class=\"position-absolute\" style=\"bottom: 15px;\"><a class=\"btn btn-primary\" href=\"./index.html\">Спробувати ще раз</a></div></div>";
    if (response['Error'] === 'Invalid date') {
      document.getElementById('errortext').innerText = "Невірна дата";
    }
    else if (response['Error'] === 'This date is reserved') {
      document.getElementById('errortext').innerText = "Цей час вже зайнятий";
    }
    else {
      document.getElementById('errortext').innerText = "Невідома помилка";
    }
  }
  else {
    regSection.innerHTML = "<div style=\"min-width: 350px; min-height: 575.5px;\" class=\"d-flex flex-column align-items-center bg-light rounded shadow-lg position-relative\"><legend class=\"text-center border-bottom shadow-sm text-success\">Електроний запис зроблено!</legend><form class=\"ms-5 me-5\" id=\"RegForm\"><div class=\"mb-2\"><label for=\"Name\" class=\"form-label\">П.І.Б</label><input readonly id=\"Name\" type=\"text\" class=\"form-control\"></div><div class=\"mb-2\"><label for=\"PhoneNumber\" class=\"form-label\">Номер телефону</label><input readonly id=\"PhoneNumber\" type=\"text\" class=\"form-control\"></div><div class=\"mb-2\"><label for=\"Select-Doctor\" class=\"form-label\">Сімейний лікар</label><input readonly id=\"Select-Doctor\" type=\"text\" class=\"form-control\"></div><div class=\"mb-2\"><label for=\"Select-Date\" class=\"form-label\">День</label><input readonly id=\"Select-Date\" class=\"form-control\"></select></div><div class=\"mb-3\"><label for=\"Select-Time\" class=\"form-label\">Час</label><input readonly id=\"Select-Time\" class=\"form-control\"></div><div class=\"mb-3\"><label for=\"UniqueId\" class=\"form-label\">Номер запису</label><input readonly id=\"UniqueId\" class=\"form-control\"></div><div class=\"mb-3\"><label for=\"SecretCode\" class=\"form-label\">Секретний код</label><input readonly id=\"SecretCode\" class=\"form-control\"></div></form></div>"
    document.getElementById("Name").value = response['Data']['FIO'];
    document.getElementById("PhoneNumber").value = response['Data']['PhoneNumber'];
    document.getElementById("Select-Doctor").value = response['Data']['DoctorFIO'];
    document.getElementById("Select-Date").value = response['Data']['Date'];
    document.getElementById("Select-Time").value = response['Data']['Time'];
    document.getElementById("UniqueId").value = response['Data']['UniqueId'];
    document.getElementById("SecretCode").value = response['Data']['Secret'];
  }
};