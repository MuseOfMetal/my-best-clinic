﻿namespace MyBestClinic.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public string FIO { get; set; }
        public string PhoneNumber { get; set; }
        public Doctor Doctor { get; set; }
        public DateTime SelectedDateTime { get; set; }
        public int UniqueId { get; set; }
        public string Secret { get; set; }
    }
}
