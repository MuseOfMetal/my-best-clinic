﻿using System.Text.Json.Serialization;

namespace MyBestClinic.Models
{
    public class Doctor
    {
        public int Id { get; set; }
        public string FIO { get; set; }
        [JsonIgnore]
        public List<Ticket>? Tickets { get; set; }
    }
}
