﻿namespace MyBestClinic.Models
{
    public class SuccessfullCreatedTicket
    {
        public string FIO { get; set; }
        public string PhoneNumber { get; set; }
        public string DoctorFIO { get; set; }
        public DateTime SelectedDateTime { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public int UniqueId { get; set; }
        public string Secret { get; set; }
    }
}
