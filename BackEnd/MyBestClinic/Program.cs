using MyBestClinic;
using MyBestClinic.Models;
using System.Globalization;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;

object locker = new object();

var builder = WebApplication.CreateBuilder(args);

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.AllowAnyOrigin();
                          policy.AllowAnyHeader();
                      });
});


// Add services to the container.

var app = builder.Build();

app.UseCors(MyAllowSpecificOrigins);
// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

string[] allowedTimes = { "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "17:15", "17:30", "17:45", "18:00" };


app.MapGet("/getdoctors", () =>
{
    using (var db = new MyBestClinicDbContext())
    {
        return new Response() { Success = true, Data = db.Doctors.ToArray() };
    }
});

app.MapGet("/getreservedtimes", (int docId, string date) =>
{
    Doctor doctor;
    List<Ticket> ticketList;
    using (var db = new MyBestClinicDbContext())
    {
        doctor = db.Doctors.Where(doc => doc.Id == docId).First();
        ticketList = db.Tickets.ToList();
    }
    ticketList = ticketList.Where(t => t.SelectedDateTime.ToString("yyyy-MM-dd") == date).ToList();
    var _allowedTimes = allowedTimes.ToList();
    foreach (var item in ticketList)
    {
        _allowedTimes.Remove(item.SelectedDateTime.ToString("HH:mm"));;
    }
    return _allowedTimes;
});

app.MapPost("/createTicket", async (HttpRequest request) =>
{
    try
    {
        lock (locker)
        {
            var form = request.Form.ToDictionary(k => k.Key);
            Ticket ticket = new Ticket();
            ticket.FIO = form["Name"].Value;
            ticket.PhoneNumber = form["PhoneNumber"].Value;
            if (CheckDate(form["Select_Date"].Value, form["Select_Time"].Value))
                ticket.SelectedDateTime = DateTime.ParseExact($"{form["Select_Date"].Value} {form["Select_Time"].Value}", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            else
                return ConvertToJson(new Response() { Success = false, Error = "Invalid date" });
            using (var db = new MyBestClinicDbContext())
            {
                Doctor doctor = db.Doctors.SingleOrDefault(doc => doc.Id == int.Parse(form["Select_Doctor"].Value));
                if (doctor == null)
                    return ConvertToJson(new Response() { Success = false, Error = "Doctor not found" });
                
                if (db.Tickets.Where(t=> t.SelectedDateTime == ticket.SelectedDateTime).ToArray().Length > 0)
                {
                    return ConvertToJson(new Response() { Success = false, Error = "This date is reserved" });
                }
                ticket.Doctor = doctor;
                ticket.Secret = Guid.NewGuid().ToString("d").Substring(1, 8);
                int randomId;
                do
                {
                    randomId = new Random().Next(100000000, int.MaxValue);
                    global::System.Console.WriteLine(randomId);
                }
                while (db.Tickets.Where(i => i.UniqueId == randomId).ToArray().Length != 0);
 
                ticket.UniqueId = randomId;
                if (doctor.Tickets == null)
                    doctor.Tickets = new List<Ticket>();
                doctor.Tickets.Add(ticket);
                db.Tickets.Add(ticket);
                db.SaveChanges();
            }

            return ConvertToJson( new Response() { Success = true, Data = new SuccessfullCreatedTicket() 
            {
                DoctorFIO = ticket.Doctor.FIO,
                Secret = ticket.Secret,
                FIO = ticket.FIO,
                PhoneNumber = ticket.PhoneNumber,
                SelectedDateTime = ticket.SelectedDateTime,
                UniqueId = ticket.UniqueId,
                Date = ticket.SelectedDateTime.ToString("dd.MM.yyyy"),
                Time = ticket.SelectedDateTime.ToString("HH:mm"),
            }
            });
        }
    }
    catch (Exception ex)
    {
        global::System.Console.WriteLine(ex);
        return ConvertToJson(new Response() { Success = false, Error = ex.ToString() });
    }
});

app.Run();

string ConvertToJson(object obj)
{
    return JsonConvert.SerializeObject(obj, Formatting.Indented,
                new JsonSerializerSettings
                {

                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
}
bool CheckDate(string date, string time)
{
    try
    {
        DateTime dateTime = DateTime.ParseExact($"{date} {time}", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
        return (dateTime > DateTime.Now) && allowedTimes.Contains(time);
    }
    catch
    {
        return false;
    }
}