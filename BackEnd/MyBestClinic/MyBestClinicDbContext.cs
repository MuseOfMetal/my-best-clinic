﻿using Microsoft.EntityFrameworkCore;
using MyBestClinic.Models;

namespace MyBestClinic
{
    public class MyBestClinicDbContext : DbContext
    {
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Doctor> Doctors { get; set; }

        public MyBestClinicDbContext()
        {
            if (Database.EnsureCreated())
            {
                //Adding doctors
                Doctors?.AddRange(
                    new Doctor()
                    {
                        FIO = "Фірсов Микита Матвійович",
                        Tickets = new List<Ticket>()
                    },
                    new Doctor()
                    {
                        FIO = "Самойлов Ілля Вікторович",
                        Tickets = new List<Ticket>()
                    }
                    ,
                    new Doctor()
                    {
                        FIO = "Єрмолов Артем Сергійович",
                        Tickets = new List<Ticket>()
                    }
                    );
                SaveChanges();
            }
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            using (StreamReader r = new StreamReader("connstring.txt"))
                optionsBuilder.UseMySQL(r.ReadToEnd());
        }
    }
}
